package reteNeurale;

import java.io.Serializable;
import java.util.Random;

public class Neurone implements Serializable {

    private double[] pesi=new double[10];
    private Random generatore=new Random();
    private double bias;

    public Neurone() {
        initpesi();
    }
    public Neurone(double[] inputPesi){
        pesi=inputPesi;
        bias=Math.random()-Math.random();
    }

    public Neurone(double[] inputPesi, double inputBias){
        pesi=inputPesi;
        bias=inputBias;
    }

    public double Attiva(double[] input){
        double somma=0;
        for(int i=0;i<input.length;i++){
            somma+=pesi[i]*input[i];
        }
        somma+=bias;
        double attiv=1/(1+Math.pow(Math.E,-somma));         //funzione di attivazione
        return attiv;
    }

    public void initpesi() {
        for (int i = 0; i < pesi.length; i++) {
            pesi[i] = Math.random()-Math.random();
        }
        bias=Math.random()-Math.random();
    }

    public double[] miglioraPesi(){
        int n=generatore.nextInt(pesi.length);
        double[] newPesi=new double[pesi.length];
        for (int i = 0; i < pesi.length; i++) {
            if(i==n)
                newPesi[i] = (random(-1,1));
            else
                newPesi[i]=pesi[i];
        }
        return newPesi;
    }


    public double[] copiaPesiNeurone(){
        double[] newPesi=new double[pesi.length];
        for (int i = 0; i < pesi.length; i++)
                newPesi[i]=pesi[i];
        return newPesi;
    }


    public static double random( double min, double max )
    {
        double diff = max - min;
        return min + Math.random( ) * diff;
    }

    public double getBias() {
        return bias;
    }

}
