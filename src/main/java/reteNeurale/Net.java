package reteNeurale;

import java.io.Serializable;
import java.util.Random;

public class Net implements Serializable {          //implemento l'interfaccia per poter salvare un oggetto rete su file esterno e poi ricaricarlo

    private static final int NUM_OUT=3,NUM_MIDDLE=10;
    private Neurone[] middle=new Neurone[NUM_MIDDLE];
    private Neurone[] out=new Neurone[NUM_OUT];
    private int fitness;
    private static Random gen=new Random();

    public Net(){
        for(int i=0;i<middle.length;i++){
            Neurone n=new Neurone();
            middle[i]=n;
        }
        for(int i=0;i<out.length;i++){
            Neurone n=new Neurone();
            out[i]=n;
        }
    }

    public Net(Neurone[] mid,Neurone[] output){
        middle=mid;
        out=output;
    }


    public int Attiva(double[] input){
        double[] newinput = new double[14];
        double[] output = new double[4];
        for(int i=0;i<middle.length-1;i++){
            newinput[i]=middle[i].Attiva(input);
        }
        for(int i=0;i<4;i++){
            output[i]=out[i].Attiva(newinput);
        }
        if(output[0]>output[1] && output[0]>output[2] && output[0]>output[3])       //Direzione output
            return 0;
        if(output[1]>output[2] && output[1]>output[3])
            return 1;
        if(output[2]>output[3])
            return 2;
        return 3;
    }

    public int Attiva1Layer(double[] input){
        double[] output = new double[NUM_OUT];
        double[] input2 = new double[NUM_MIDDLE];
        for(int i=0;i<NUM_MIDDLE;i++){
            input2[i]=middle[i].Attiva(input);
        }
        for(int i=0;i<NUM_OUT;i++){
            output[i]=out[i].Attiva(input2);
        }
        if(output[0]>output[1] && output[0]>output[2])       //Direzione output
            return 0;
        if(output[1]>output[2])
            return -1;
        return 1;
    }

    public Net miglioraPesi(){
        Neurone[] newMiddle=new Neurone[NUM_MIDDLE];
        Neurone[] newOut=new Neurone[NUM_OUT];
        for(int i=0;i<middle.length;i++){
            newMiddle[i]=new Neurone(middle[i].miglioraPesi(),middle[i].getBias()+(Math.random()-0.5));
        }
        for(int i=0;i<out.length;i++){
            newOut[i]=new Neurone(out[i].miglioraPesi(),out[i].getBias()+(Math.random()-0.5));
        }
        return new Net(newMiddle,newOut);
    }

    public static Net unisciReti(Net net, Net net1) {
        Neurone[] nout=new Neurone[NUM_OUT];
        Neurone[] nmiddle=new Neurone[NUM_MIDDLE];
        int partenzaUnireMiddle=gen.nextInt(10);
        int lunghezzaUnireMiddle=gen.nextInt(9);
        int partenzaNeuroneUnire=gen.nextInt(3);
        int lunghezzaNeuroniUnire=gen.nextInt(2);
        int i=0;
        for(i=0;i<partenzaNeuroneUnire;i++){
            nout[i]=new Neurone(net.out[i].copiaPesiNeurone());
        }
        nout[i]=new Neurone(net1.out[i].copiaPesiNeurone());
        i++;
        if(lunghezzaNeuroniUnire==1 && i<3){
            nout[i]=new Neurone(net1.out[i].copiaPesiNeurone());
            i++;}
        while(i<3){
            nout[i]=new Neurone(net.out[i].copiaPesiNeurone());
            i++;
        }
        for(i=0;i<partenzaUnireMiddle;i++){
            nmiddle[i]=new Neurone(net.middle[i].copiaPesiNeurone());
        }
        nmiddle[i]=new Neurone(net1.middle[i].copiaPesiNeurone());
        i++;
        if(lunghezzaUnireMiddle==1 && i<10){
            nmiddle[i]=new Neurone(net1.middle[i].copiaPesiNeurone());
            i++;}
        while(i<10){
            nmiddle[i]=new Neurone(net.middle[i].copiaPesiNeurone());
            i++;
        }

        return new Net(nmiddle,nout);
    }

    public void setFitness(int f){fitness=-f;}

    public int getFitness(){return fitness;}

}
