package gioco;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.IOException;

public class Campo {

    private JFrame frame;
    private Canvas canvas;
    private JPanel panel,panel2;
    private JButton bott,esporta;
    private Gestore ges;
    private JLabel label,statlabel;
    private JTextField text;

    public Campo(int w, int h,Gestore G){
        ges=G;
        frame=new JFrame("SNAKE INTELLIGENTE");
        panel= new JPanel(new BorderLayout());
        canvas=new Canvas();
        label= new JLabel("FPS");
        statlabel=new JLabel();
        text=new JTextField();
        bott=new JButton("Invio");
        esporta= new JButton("Esporta Snake");
        panel2=new JPanel(new FlowLayout());

        panel2.setPreferredSize(new Dimension(1000,30));
        panel2.setBackground(Color.BLACK);
        label.setForeground(Color.blue);
        statlabel.setForeground(Color.white);
        //frame.setSize(new Dimension(w,h+30));
        //frame.setPreferredSize(new Dimension(w,h+30));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
        canvas.setSize(new Dimension(w,h));
        canvas.setPreferredSize(new Dimension(w,h));
        esporta.setBackground(Color.BLACK);
        esporta.setForeground(Color.white);

        panel2.add(statlabel, FlowLayout.LEFT);
        panel2.add(bott,FlowLayout.CENTER);
        panel2.add(text,FlowLayout.CENTER);
        panel2.add(label, FlowLayout.CENTER);
        panel2.add(esporta, FlowLayout.RIGHT);
        panel.add(panel2,BorderLayout.NORTH);
        panel.add(canvas);
        frame.add(panel);

        text.setText(String.valueOf(60));
        bott.addActionListener(actionEvent -> {ges.setFps(Integer.parseInt(text.getText()));});
        esporta.addActionListener(actionEvent -> {ges.salvaConfNet();});

        frame.pack();
    }

    public void setTitle(String l){
        frame.setTitle(l);
    }

    public void setStatlabel(String l){
        statlabel.setText(l);
    }

    public Canvas getCanvas() {
        return canvas;
    }


}
