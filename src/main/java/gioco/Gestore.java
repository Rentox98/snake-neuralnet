package gioco;

import componenti.Cibo;
import componenti.Snake;
import reteNeurale.Net;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.io.*;
import java.util.ArrayList;

public class Gestore {
    private Snake serpente;
    private Cibo cibo;
    private Campo campo;
    private Graphics g;
    private BufferStrategy bs;
    private ArrayList<Integer> generazione;
    private int fps=60;
    private final int WIDTH=1000,HEIGHT=600;

    public Gestore(){
        campo=new Campo(WIDTH,HEIGHT,this);
        serpente=new Snake(WIDTH/2,HEIGHT/2);
        cibo=new Cibo(WIDTH-Cibo.getWIDTH(),HEIGHT-Cibo.getHEIGH());
    }

    public void tick(){
        generazione=serpente.tick(cibo);
        campo.setStatlabel("Individuo : "+generazione.get(0)+" Cibo Mangiato : "+generazione.get(2)+" Record: "+generazione.get(1));
    }

    public void render(){
        bs = campo.getCanvas().getBufferStrategy();
        if(bs == null) {
            campo.getCanvas().createBufferStrategy(2); // disegna su 2 buffer prima di arrivare al canvas
            return;
        }
        g =  bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, WIDTH, HEIGHT);	// sfondo nero
        g.setColor(Color.blue);
        serpente.render(g);
        g.setColor(Color.white);
        cibo.render(g);

        bs.show();
        g.dispose(); // libera le risorse usate dall'oggetto graphics, da fare a fine disegno

    }
    public void setFps(int fps) {
        this.fps=fps;
    }

    public static void main(String[] args) throws IOException {
        Gestore gioco=new Gestore();
        gioco.fps = 60;
        double timePerTick = 1000000000 / gioco.fps; // 1 secondo fratto fps(in nanosecondi)
        double delta = 0; // tempo passato
        long now;
        long lastTime = System.nanoTime(); // current time

        while(true){
            now = System.nanoTime();
            timePerTick=1000000000 / gioco.fps;
            delta += (now - lastTime) / timePerTick; //  tempo passato dall'esecuzione precedente, fratto quanto è il tempo massimo da aspettare
            lastTime = now;
            if (delta >= 1) {



                gioco.tick();          //esegue il gioco
                gioco.render();

                delta--;
            }

        }

    }

    public void decrEpsilon(){
        serpente.setEpsilon(serpente.getEpsilon()-0.05);
    }

    public void salvaConfNet() {
        try
        {
            String nomefile = "snake";
            FileOutputStream fos = new FileOutputStream(nomefile);
            ObjectOutputStream oos=new ObjectOutputStream(fos);             //permette di salvare oggetti su file
            oos.writeObject(serpente);
            //PrintStream ps = new PrintStream(fos);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
