package componenti;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Cibo {
    private int x;
    private int y;
    private static final int WIDTH=10,HEIGH=10;
    Random gen=new Random();
    private BufferedImage imgCibo;

    public Cibo(int maxX,int maxY){
        genera(maxX,maxY);
        imgCibo=CaricaImg.loadImg("./src/main/resources/10X10Cibo.png");
    }

    public void genera(int maxX,int maxY){
        x=(gen.nextInt(maxX/10))*10;
        y=(gen.nextInt(maxY/10))*10;
    }

    public void render(Graphics g) {
        //g.fillRect(x,y,WIDTH,HEIGH);
        g.drawImage(imgCibo,x,y,null);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGH() {
        return HEIGH;
    }
}
