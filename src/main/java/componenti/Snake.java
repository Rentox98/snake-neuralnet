package componenti;

import reteNeurale.Net;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class Snake implements Serializable {

    private HashMap< String, ArrayList<Integer> > Q;
    private double epsilon=0.80,gamma=0.50;
    private int direzione=1, correzione;
    private int gen=0, num_individuo =0;
    private PezziCorpo testa,coda;
    private final int WIDTH = 10,HEIGHT=10;     //dell'unità
    private ArrayList<PezziCorpo> corpo;
    private int flag=1;
    private Random gener=new Random();
    private double vita=500;
    private int ciboMangiato=0,ciboMaxmangiato=0;
    private BufferedImage imgTesta,imgCorpo;
    ArrayList<Integer> appoggio;



    public Snake(int x,int y) {
        Q=new HashMap<>();
        appoggio=new ArrayList<>();
        corpo= new ArrayList<>();
        testa=new PezziCorpo(x,y);
        coda=testa;
        corpo.add(testa);
        imgTesta =CaricaImg.loadImg("./src/main/resources/10X10Testa.png");
        imgCorpo=CaricaImg.loadImg("./src/main/resources/10X10Corpo.png");
    }

    public ArrayList<Integer> tick(Cibo cibo) {
        testa=corpo.get(0);
        int nextMax=0;
        String nextChiave="";
        int direzione_cibo=this.calcolaDirezCibo(cibo);     //vedo in che posizione relativa è il cibo
        String chiave=this.calcolaValoreMappa(direzione_cibo);
        direzione=calcolaDirezMov(chiave);
        correzione=this.cammina(cibo);

        int cenx=testa.getX()+WIDTH/2;
        int ceny=testa.getY()+HEIGHT/2;
        int cencx=cibo.getX()+cibo.getWIDTH()/2;
        int cency=cibo.getY()+cibo.getHEIGH()/2;
        int tocco=WIDTH/2+cibo.getWIDTH()/2;


        if(Math.sqrt((cenx-cencx)*(cenx-cencx)+(ceny-cency)*(ceny-cency))<=tocco){          //con il teorema di pitagora controllo le collisioni
            cibo.genera(990,590);
            int c=0;
            while(c<corpo.size()){
                if(corpo.get(c).getX()==cibo.getX() && corpo.get(c).getY()==cibo.getY()){
                    c=0;
                    cibo.genera(990,590);
                }
            else
                c++;}
            correzione=50;
            coda=corpo.get(corpo.size()-1);
            corpo.add(new PezziCorpo(coda.getX(),coda.getY()));
            flag=2;                                 //il flag lo uso per vedere se la coda nuova deve rimanere ferma e quindi non essere nel for
            vita+=400;
            ciboMangiato++;
            direzione_cibo=this.calcolaDirezCibo(cibo);
        }
        if(corpo.get(0).getX()>990 || corpo.get(0).getX()<0 || corpo.get(0).getY()>590 || corpo.get(0).getY()<0 || vita<=0 || this.checkCollisioneCorpo()){
            corpo=new ArrayList<>();
            corpo.add(new PezziCorpo(500,250));
            cibo.genera(990,590);
            gestisciGen();
            vita=500;
            correzione=-50;
        }
        for(int i=corpo.size()-flag;i>0;i--){
            corpo.get(i).setX(corpo.get(i-1).getX());
            corpo.get(i).setY(corpo.get(i-1).getY());
        }
        flag=1;
        testa=corpo.get(0);

        nextChiave=this.calcolaValoreMappa(direzione_cibo);
        if(Q.containsKey(nextChiave))
            nextMax=trovaNextMax(Q.get(nextChiave));
        appoggio=Q.get(chiave);
        appoggio.set(direzione, (int) (correzione+(gamma*nextMax)));
        Q.put(chiave,appoggio);


        vita--;
        ArrayList<Integer> ret=new ArrayList<>();
        ret.add(num_individuo);
        ret.add(ciboMaxmangiato);
        ret.add(ciboMangiato);
        return ret;
    }

    private boolean checkCollisioneCorpo() {
        for(int i=2;i<corpo.size();i++){
            if(testa.getX()==corpo.get(i).getX()&&testa.getY()==corpo.get(i).getY())
                return true;
        }
        return false;
    }

    public int trovaNextMax(ArrayList<Integer> a){
        int max=a.get(0);
        for(int i=1;i<a.size();i++){
            if(a.get(i)>max)
                max=a.get(i);
        }
        return max;
    }

    public void gestisciGen(){
        num_individuo++;
        if(ciboMangiato>ciboMaxmangiato)
            ciboMaxmangiato=ciboMangiato;
        ciboMangiato=0;
        if((num_individuo+1)%10==0)
            epsilon-=0.05;

    }
    public int calcolaDirezCibo(Cibo cibo){
        int direzione_cibo;
        int xrel=cibo.getX()-testa.getX();
        int yrel=cibo.getY()-testa.getY();
        if(xrel<0)
            if(yrel<0)
                direzione_cibo=0;
            else
            if(yrel==0)
                direzione_cibo=7;
            else
                direzione_cibo=6;
        else
        if(xrel>0)
            if(yrel>0)
                direzione_cibo=4;
            else
            if(yrel==0)
                direzione_cibo=3;
            else
                direzione_cibo=2;
        else
        if(yrel>0)
            direzione_cibo=5;
        else
            direzione_cibo=1;
        return direzione_cibo;
    }

    public String calcolaValoreMappa(int direzione_cibo){
        int ostacoloSx=0,ostacoloDx=0,ostacoloUp=0,ostacoloDown=0;

        if(testa.getX()==0 || direzione==2)
            ostacoloSx = 1;
        else
            for(int i=2;i<corpo.size();i++)
                if (testa.getX()-10==corpo.get(i).getX() && corpo.get(i).getY()<=testa.getY()+10 && corpo.get(i).getY()>=testa.getY())
                    ostacoloSx = 1;

        if(testa.getY()==0 || direzione==3)
            ostacoloUp = 1;
        else
            for(int i=2;i<corpo.size();i++)
                if (testa.getY()-10==corpo.get(i).getY() && corpo.get(i).getX()<=testa.getX()+10 && corpo.get(i).getX()>=testa.getX())
                    ostacoloUp = 1;

        if(testa.getX()==990 || direzione==0)
            ostacoloDx = 1;
        else
            for(int i=2;i<corpo.size();i++)
                if (testa.getX()+10==corpo.get(i).getX() && corpo.get(i).getY()<=testa.getY()+10 && corpo.get(i).getY()>=testa.getY())
                    ostacoloDx = 1;

        if(testa.getX()==590 || direzione==1)
            ostacoloDown = 1;
        else
            for(int i=2;i<corpo.size();i++)
                if (testa.getY()+10==corpo.get(i).getY() && corpo.get(i).getX()<=testa.getX()+10 && corpo.get(i).getX()>=testa.getX())
                    ostacoloDown = 1;


        String colonna="";
        colonna=direzione_cibo+""+ostacoloUp+""+ostacoloDown+""+ostacoloDx+""+ostacoloSx+colonna;

        return colonna;
    }

    public int calcolaDirezMov(String campo){
        ArrayList<Integer> riga=new ArrayList<>();
        riga.add(0);riga.add(0);riga.add(0);riga.add(0);
        if(Math.random()<epsilon){              //fattore di esplorazione
            if(!Q.containsKey(campo))
                Q.put(campo,riga);
            direzione=gener.nextInt(3);
        }
        else{
            if(Q.containsKey(campo)) {
                riga=Q.get(campo);
                if(riga.get(0)>riga.get(1) && riga.get(0)>riga.get(2) && riga.get(0)>riga.get(3))
                    direzione=0;
                else
                    if(riga.get(1)>riga.get(2) && riga.get(1)>riga.get(3))
                        direzione=1;
                    else
                        if(riga.get(2)>riga.get(3))
                            direzione=2;
                        else
                            direzione=3;
            }
            else {
                Q.put(campo,riga);
                direzione=gener.nextInt(3);
            }
        }
        return direzione;
    }

    public int cammina(Cibo cibo){
        int correzione=0;
        switch(direzione){
            case 0:
                if(testa.getX()>cibo.getX())
                    correzione++;
                else
                    correzione--;
                corpo.get(0).setX(corpo.get(0).getX()-10);   //Movimento testa sx
                break;
            case 1:
                if(testa.getY()>cibo.getY())
                    correzione++;
                else
                    correzione--;
                corpo.get(0).setY(corpo.get(0).getY()-10);   //Movimento testa up
                break;
            case 2:
                if(testa.getX()<cibo.getX())
                    correzione++;
                else
                    correzione--;
                corpo.get(0).setX(corpo.get(0).getX()+10);   //Movimento testa dx
                break;
            case 3:
                if(testa.getY()<cibo.getY())
                    correzione++;
                else
                    correzione--;
                corpo.get(0).setY(corpo.get(0).getY()+10);   //Movimento testa down
                break;
        }
        return correzione;
    }

    public void render(Graphics g) {
        g.setColor(Color.green);
        g.drawImage(imgTesta,testa.getX(),testa.getY(),null);
        //g.fillRect(testa.getX(),testa.getY(),WIDTH,HEIGHT);
        g.setColor(Color.blue);
        for (int i=2; i<corpo.size();i++) {
            g.drawImage(imgCorpo,corpo.get(i).getX(),corpo.get(i).getY(),null);
            //g.fillRect(corpo.get(i).getX(),corpo.get(i).getY(),WIDTH,HEIGHT);
        }
    }


    public void setEpsilon(double i) {
        epsilon=i;
    }

    public double getEpsilon() {
        return epsilon;
    }
}
